import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {

  services: any[] = [];

  constructor() { }

  ngOnInit() {
    this.services = [
      {
        description: "Desarrollo de aplicaciones móviles",
        image: "assets/images/services/app_moviles.jpg"
      },
      {
        description: "Desarrollo de páginas web",
        image: "assets/images/services/pags_web.jpg"
      },
      {
        description: "Sistemas de seguridad vehicular",
        image: "assets/images/services/seguridad_vehicular.jpg"
      },
      {
        description: "Sistema domótico",
        image: "assets/images/services/domotica.jpg"
      },
      {
        description: "Desarrollo de videojuegos",
        image: "assets/images/services/videojuegos.jpg"
      },
      {
        description: "Manejo de redes sociales",
        image: "assets/images/services/redes_sociales.jpg"
      },
      {
        description: "Diseño de imagen corporativa",
        image: "assets/images/services/imagen_corporativa.jpg"
      },
      {
        description: "Estrategias de marketing",
        image: "assets/images/services/marketing.jpg"
      },
      {
        description: "Automatización de sistemas",
        image: "assets/images/services/automatizacion.jpg"
      }
    ];
  }

}
