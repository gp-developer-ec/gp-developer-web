import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  planes: string[] = [];
  paquetes: string[] = [];

  constructor() { }

  ngOnInit() {
    this.planes = [
      "assets/images/planes/basico.png",
      "assets/images/planes/emprendedor.png",
      "assets/images/planes/premium.png"
    ];

    this.paquetes = [
      "assets/images/paquetes/web_landing.jpg",
      "assets/images/paquetes/web_profesional.jpg",
      "assets/images/paquetes/web_pyme.jpg",
      "assets/images/paquetes/web_premium.jpg",
      "assets/images/paquetes/app_basica.jpg",
      "assets/images/paquetes/app_emprendedor.jpg",
      "assets/images/paquetes/app_premium.jpg",
    ];
  }

}
