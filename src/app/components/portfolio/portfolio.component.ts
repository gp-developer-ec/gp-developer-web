import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {

  clients: any[] = [];
  colaboraciones: any[] = [];

  constructor() { }

  ngOnInit() {
    this.clients = [
      {
        image: "assets/images/portfolio/soy_vip.png"
      },
      {
        image: "assets/images/portfolio/orto_dental.png"
      },
      {
        image: "assets/images/portfolio/wyn.png"
      }
    ];

    this.colaboraciones = [
      {
        image: "assets/images/portfolio/fundacion_down.png"
      }
    ];
  }


}
