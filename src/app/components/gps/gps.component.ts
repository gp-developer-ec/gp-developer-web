//http://localhost:4200/gps?lat=0.7547&lon=-47.44777&cod=dede44de&fec=enero
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { FirebaseService } from "../../servicios/firebase.service";
import { Modulo } from 'src/app/modelos/modulo';

@Component({
  selector: 'app-gps',
  templateUrl: './gps.component.html',
  styleUrls: ['./gps.component.scss']
})
export class GpsComponent implements OnInit {
  lat: string = "";
  lon: string = "";
  fec: string = "";
  cod: string = "";
  aux_crear_o_modificar: boolean = false;
  infoModulos: Modulo;
  constructor(
    private route: ActivatedRoute,
    private firebase: FirebaseService
  ) {
  }

  ngOnInit() {

    this.route.queryParamMap.subscribe(queryParams => {
      this.cod = queryParams.get("cod");
      this.lat = queryParams.get("lat");
      this.lon = queryParams.get("lon");
      this.fec = queryParams.get("fec");

      if (this.cod == null || this.lat == null || this.lon == null || this.fec == null) {
        console.log("No se puede ingresar datos por falta de una o varias variables");
      } else {
        console.log("Se ingreso coordenadas al modulo");
        // this.firebase.subirInformacionModulo(this.cod, this.lat, this.lon, new Date);
        this.firebase.subirCoordenadasModulo(this.cod, this.lat, this.lon, new Date);
      }

    })
  }
  

}
