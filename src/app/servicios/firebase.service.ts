import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreCollectionGroup } from "@angular/fire/firestore";
import { Modulo } from '../modelos/modulo';
import { map } from "rxjs/operators";
import * as firebase from 'firebase/app'

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  constructor(
    public afs: AngularFirestore,
  ) {


  }
  subirInformacionModulo(cod: string, lat: string, lon: string, fec: Date) {
    const infoModulo: Modulo = {
      id: cod,
      historial: [],
      lat: lat,
      lon: lon,
      fec: fec,
    }
    return this.afs.collection('GPS').doc(infoModulo.id).set(infoModulo);
  }
  subirCoordenadasModulo(cod: string, lat: string, lon: string, fec: Date) {
    const informacionLocacion = new firebase.firestore.GeoPoint(+lat, +lon);
    const infoModulo: Modulo = {
      id: cod,
      historial: [],
      lat: lat,
      lon: lon,
      fec: fec,
    }
    return this.afs.collection('GPS').doc(infoModulo.id).update(
      {
        historial_posicion:firebase.firestore.FieldValue.arrayUnion(informacionLocacion),
        historial_fecha:firebase.firestore.FieldValue.arrayUnion(fec),
      }
      );
  }

  obtenerInformacionModulos() {
    return this.afs.collection('GPS').snapshotChanges().pipe(map(info => {
      return info.map(a => {
        const data = a.payload.doc.data() as Modulo;
        data.id = a.payload.doc.id;
        // console.log(data);
        return data;
      })
    }));
  }
}
